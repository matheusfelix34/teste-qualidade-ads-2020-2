package br.ucsal.bes20202.testequalidade.aula12;

import org.junit.Assert;
import org.junit.Test;

public class CalculoUtilTest {

	@Test
	public void testarFatorial5() {
		Integer n = 5;
		Long fatorialEsperado = 120L;

		Long fatorialAtual = CalculoUtil.calcularFatorial(n);

		Assert.assertEquals(fatorialEsperado, fatorialAtual);
	}

}
