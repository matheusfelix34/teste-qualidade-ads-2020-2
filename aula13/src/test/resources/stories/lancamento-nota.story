Narrativa:
Como um professor
desejo informar as duas notas de um aluno
de modo que possa saber se o mesmo foi aprovado

Cenário: Aluno aprovado com nota superior à média
Dado que um aluno está matriculado na disciplina
Quando informo a nota 8
E informo a nota 9
Então a situação do aluno é Aprovado
E a média é 8.5

Cenário: Aluno reprovado com nota inferior à média
Dado que um aluno está matriculado na disciplina
Quando informo a nota 3
E informo a nota 5
Então a situação do aluno é Reprovado
E a média é 4

Cenário: Aluno aprovado com nota na média
Dado que um aluno está matriculado na disciplina
Quando informo a nota 8
E informo a nota 4
Então a situação do aluno é Aprovado
E a média é 6
