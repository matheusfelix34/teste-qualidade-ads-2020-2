package br.ucsal.bes20202.testequalidade.aula13;

import java.util.ArrayList;
import java.util.List;

public class Aluno {
	
	private List<Double> notas;

	public Aluno() {
		notas = new ArrayList<>();
	}

	public void informarNota(Double nota) {
		notas.add(nota);
	}

	public Double calcularMedia() {
		Double soma = 0d;
		for (Double nota : notas) {
			soma += nota;
		}
		return soma / notas.size();
	}

	public String obterSituacao() {
		if (calcularMedia() >= 6) {
			return "Aprovado";
		} else {
			return "Reprovado";
		}
	}

}
