package br.ucsal.bes20202.testequalidade.aula14;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public abstract class InvestingComAbstractTest {

	private WebDriver driver;

	@Before
	public void setup() {
		driver = getSeleniumDriver();
	}

	protected abstract WebDriver getSeleniumDriver();

	@After
	public void teardown() {
		driver.quit();
	}

	@Test
	public void testarPesquisa() throws InterruptedException {
		// Abrir página do Investing.com
		driver.get("http://br.investing.com");

		// Preencher o input de "Pesquisar no site..."
		WebElement pesquisarNoSiteInput = driver.findElement(By.className("searchText"));
		pesquisarNoSiteInput.sendKeys("COGN3" + Keys.ENTER);

		// Obter o conteúdo da página
		Thread.sleep(5000);
		String conteudo = driver.getPageSource();

		// Verificar se retorno inclui "Cogna Educação"
		Assert.assertTrue(conteudo.contains("Cogna Educacao"));

	}

}
