package br.ucsal.bes20202.testequalidade.aula14;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class PesquisaAtivoSteps {

	private WebDriver driver;

	@Given("estou no $nomeBrowser")
	public void abrirBrowser(String nomeBrowser) {
		if (nomeBrowser.equalsIgnoreCase("Firefox")) {
			System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver");
			driver = new FirefoxDriver();
		} else if (nomeBrowser.equalsIgnoreCase("Chrome")) {
			System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver");
			driver = new ChromeDriver();
		}
	}

	@Given("estou no site $url")
	public void abrirSite(String url) throws InterruptedException {
		driver.get(url);
	}

	@When("informo $valorPesquisa no campo de pesquisa")
	public void informarValorPesquisa(String valorPesquisa) {
		WebElement pesquisarNoSiteInput = driver.findElement(By.className("searchText"));
		pesquisarNoSiteInput.sendKeys(valorPesquisa);
	}

	@When("teclo ENTER")
	public void teclarEnter() {
		WebElement pesquisarNoSiteInput = driver.findElement(By.className("searchText"));
		pesquisarNoSiteInput.sendKeys(Keys.ENTER);
	}

	@Then("é exibido na página o texto $conteudoEsperado")
	public void verificarConteudoPagina(String conteudoEsperado) throws InterruptedException {
		Thread.sleep(5000);
		String conteudo = driver.getPageSource();
		Assert.assertTrue(conteudo.contains(conteudoEsperado));
	}

	@Then("fecho o browser")
	public void fecharBrowser() {
		driver.quit();
	}
}
