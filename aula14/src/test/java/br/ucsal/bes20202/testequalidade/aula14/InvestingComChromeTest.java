package br.ucsal.bes20202.testequalidade.aula14;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class InvestingComChromeTest extends InvestingComAbstractTest {

	@Override
	protected WebDriver getSeleniumDriver() {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver");
		return new ChromeDriver();
	}

}
