package br.ucsal.bes20202.testequalidade.aula14;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class InvestingComFirefoxTest extends InvestingComAbstractTest {

	@Override
	protected WebDriver getSeleniumDriver() {
		// System.setProperty("webdriver.firefox.bin", "path/firefox.exe");
		System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver");
		return new FirefoxDriver();
	}

}
