Narrativa:
Como um investidor
desejo pesquisar por código de ativo
de modo que possa saber o nome da empresa correspondente

Cenário: Pesquisa de código de ativo na página da InvestingCom
Dado que estou no site http://br.investing.com
Quando informo COGN3 no campo de pesquisa
E teclo ENTER
Então é exibido na página o texto Cogna Educacao
